'use strict';

const gulp = require("gulp");
const sass = require("gulp-sass");

sass.compiler = require("node-sass");

gulp.task("sass-01", function () {
    return gulp.src("app-01/scss/*.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(gulp.dest("app-01/css"));
});

gulp.task("watch", function () {
    gulp.watch("app-01/scss/*.scss", gulp.series("sass-01"))
});